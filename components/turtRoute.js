const TurtImages = {
  '01d': require('../assets/turt/meh.gif'),
  '01n': require('../assets/turt/meh.gif'),
  '02d': require('../assets/turt/meh.gif'),
  '02n': require('../assets/turt/meh.gif'),
  '03d': require('../assets/turt/meh.gif'),
  '03n': require('../assets/turt/meh.gif'),
  '04d': require('../assets/turt/meh.gif'),
  '04n': require('../assets/turt/meh.gif'),
  '09n': require('../assets/turt/rain.gif'),
  '09d': require('../assets/turt/rain.gif'),
  '10d': require('../assets/turt/rain.gif'),
  '10n': require('../assets/turt/rain.gif'),
  '11d': require('../assets/turt/storm.gif'),
  '11n': require('../assets/turt/storm.gif'),
  '13d': require('../assets/turt/storm.gif'),
  '13n': require('../assets/turt/storm.gif'),
  '50d': require('../assets/turt/meh.gif'),
  '50n': require('../assets/turt/meh.gif')
}
  
export default TurtImages