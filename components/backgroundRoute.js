const IconImages = {
  '01d': require('../assets/backgrounds/sunny.png'),
  '01n': require('../assets/backgrounds/sunny.png'),
  '02d': require('../assets/backgrounds/oneCloud.png'),
  '02n': require('../assets/backgrounds/oneCloud.png'),
  '03d': require('../assets/backgrounds/cloudy.png'),
  '03n': require('../assets/backgrounds/cloudy.png'),
  '04d': require('../assets/backgrounds/cloudy.png'),
  '04n': require('../assets/backgrounds/cloudy.png'),
  '09n': require('../assets/backgrounds/rain.png'),
  '09d': require('../assets/backgrounds/rain.png'),
  '10d': require('../assets/backgrounds/rain.png'),
  '10n': require('../assets/backgrounds/rain.png'),
  '11d': require('../assets/backgrounds/thunder.png'),
  '11n': require('../assets/backgrounds/thunder.png'),
  '13d': require('../assets/backgrounds/snow.png'),
  '13n': require('../assets/backgrounds/snow.png'),
  '50d': require('../assets/backgrounds/cloudy.png'),
  '50n': require('../assets/backgrounds/cloudy.png')
}
  
export default IconImages