import React from 'react';
import { Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
 
// const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
// const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

const api_key = 'AIzaSyC9cNy14ZdiVATcuoqDyDiRr4JypuDR3Gs';

 
function GooglePlacesInput(props) {
  return (
    <GooglePlacesAutocomplete
      placeholder='Search'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      
      onPress={ async (data, details=null) => { // 'details' is provided when fetchDetails = true
      console.log('data =====>',data);
      const resJson = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?place_id=${data.place_id}&key=${api_key}`)
      const response = await resJson.json()
      console.log('response =====>', response)
      props.getData(response.results[0].geometry.location.lat, response.results[0].geometry.location.lng)
      props.locBoolean()
  }}
 
      getDefaultValue={() => ''}
 
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyC9cNy14ZdiVATcuoqDyDiRr4JypuDR3Gs',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}
 
      styles={{
        textInputContainer: {
          width: '90%',
          borderRadius: 10,
          backgroundColor: 'darkgrey',
          marginTop: 10,
          marginLeft: '5%',
          marginRight: '5%'
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }}
 
      // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      // currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        type: 'cafe'
      }}
      
      GooglePlacesDetailsQuery={{
        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
        fields: 'formatted_address',
      }}
 
      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      // predefinedPlaces={[homePlace, workPlace]}
 
      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      // renderLeftButton={()  => {}}
      // renderRightButton={() => <Text>Custom text after the input</Text>}
    />
  );
}

export default GooglePlacesInput