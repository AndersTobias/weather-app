import React, {useState, useEffect, useCallback} from 'react';
import {SafeAreaView, FlatList, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, AsyncStorage, Alert, Picker, Image, ImageBackground, KeyboardAvoidingView, Modal, Slider, Animated, Linking, Easing} from 'react-native';
import moment from 'moment';
import { Icon, Button } from 'react-native-elements';



import IconImages from './components/imageRoute.js';
import BackgroundImages from './components/backgroundRoute.js';
import TurtImages from './components/turtRoute.js'

import LocationSearch from './components/locationSearch.js';


export default function App() {

  const [wState, setWState] = useState(null)
  const [forecastState, setForecastState] = useState(null)
  const [loading, setLoading] = useState(true)
  const [showLoc, setShowLoc] = useState(false)
  const [showAbout, setShowAbout] = useState(false)
  const [currentLoc, setCurrentLoc] = useState()
  const[sunglasses, setSunglasses] = useState(false)
  const[smiles, setSmiles] = useState(false)


  let api_key = '16909a97489bed275d13dbdea4e01f59'

  //! retrieve current location or set default for London
  useEffect(() => {
    let loc = navigator.geolocation.getCurrentPosition( coords => 
    getData(coords.coords.latitude, coords.coords.longitude))
    if(!loc) {getData(51.51, -0.13)}
  }, [])

  //! set loading

  useEffect(() => {
    if(wState !== null)setLoading(false)
    movePic()
    // console.log('wstate: ', wState)  //! Info on incomming API data 
  }, [wState])


  //! API get Data and set in State

  let getData = async (latitude, longitude) =>{
    try {
    const res1 = await fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${api_key}`)
    const res2 = await res1.json()
    const res3 = await fetch(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${latitude}&lon=${longitude}&units=metric&appid=${api_key}`)
    const res4 = await res3.json()  

    setForecastState(res4)  
    setWState(res2)
    // console.log('Daily ==>',res2, 'Forecast ==>', res4) //! Info on incomming API data 
    } catch (error) {
      console.log('getData error: ', error)
    }
  }




  //! state changers

  let locBoolean = () => {
    setShowLoc(!showLoc);
  }

  let aboutBoolean = () => {
    setShowAbout(!showAbout);
  }

  //! animation 

  const [restart, setRestart] = useState(true)

  useEffect(()=> {movePic()},[loading])

  useEffect(() => {movePic()}, [restart])

  let animationStartPoint = Math.floor(Math.random() * (Math.floor(600) - Math.ceil(470))) + Math.ceil(470); // to set a random start point from between -470 - -600 px below center (The maximum is exclusive and the minimum is inclusive)
  
  let moveAnimation = new Animated.ValueXY({ x: 380, y: animationStartPoint})



  let movePic = () => {
    Animated.timing(moveAnimation, {
      toValue: {x: -80, y: animationStartPoint},
      // easing: Easing.back(), //! IF the animation should take run up first
      duration: 15000,
    }).start(resetMovePic())
    console.log('start animation', animationStartPoint)
  }


  let resetMovePic = () => {
    setTimeout(() => setRestart(!restart), 60000);
  }

  //! tap to add


  let glassesOn = () => {
    setSunglasses(!sunglasses);
  }

  let smilesOn = () => {
    setSmiles(!smiles);
  }

  //! return 


  return (loading ? null : 
    <View style={styles.container}>
      <ImageBackground source={BackgroundImages[wState.weather[0].icon]} style={styles.backgroundImage}>
        
        {/* sunglasses */}
        {wState.weather[0].icon === '01d' && '01n' ?
        <TouchableOpacity style={styles.blank} onPress={() => glassesOn()}>
          {sunglasses === true ? <Image style={styles.sunglasses} source={require('./assets/adds/glasses.png')}/> : null}
        </TouchableOpacity>
        : null}

        {/* smiles */}
        {wState.weather[0].icon === '02d' && '02n' ?
        <TouchableOpacity style={styles.blank} onPress={() => smilesOn()}>
          {smiles === true ? 
          <View>
            <Image style={styles.smile1} source={require('./assets/adds/smiles.png')}/> 
            <Image style={styles.smile2} source={require('./assets/adds/smiles.png')}/> 
          </View>
          : null}
        </TouchableOpacity>
        : null}

        {/* icon buttons */}
        <TouchableOpacity onPress={() => locBoolean()} style={styles.earthIconLoc}>
          <Image  source={require('./assets/icons/earth.png')} style={styles.earthIcon}/>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => aboutBoolean()} style={styles.questIconLoc}>
          <Image  source={require('./assets/icons/questionmark.png')} style={styles.questIcon}/>
        </TouchableOpacity>


        {/* forecast */}
        <Text style={styles.mainText}>It's {parseInt(wState.main.temp)}º in {wState.name}... </Text>

        
        <View style={styles.forecast}>
          <View>
            <Text style={styles.forecastText}>{moment(new Date(forecastState.list[1].dt*1000)).format('ddd')} {parseInt(forecastState.list[1].temp.day)}º</Text>
            <Image style={styles.weatherIconSmall} source={IconImages[forecastState.list[1].weather[0].icon]}/>
          </View>
          <View>
            <Text style={styles.forecastText}>{moment(new Date(forecastState.list[2].dt*1000)).format('ddd')} {parseInt(forecastState.list[2].temp.day)}º</Text>
            <Image style={styles.weatherIconSmall} source={IconImages[forecastState.list[2].weather[0].icon]}/>
          </View>
          <View> 
            <Text style={styles.forecastText}>{moment(new Date(forecastState.list[3].dt*1000)).format('ddd')} {parseInt(forecastState.list[3].temp.day)}º</Text>
            <Image style={styles.weatherIconSmall} source={IconImages[forecastState.list[3].weather[0].icon]}/>
          </View> 
        </View>

        <View>
         {wState.weather[0].icon === '01d' && '01n' ? <Text style={styles.wishText}> Wish I was there.. </Text> : <Text style={styles.grassText}> The grass is always greener on the other side.. </Text>}
        </View>
        
        {/* animation */}
        <Animated.View style={[styles.turtbox, moveAnimation.getLayout()]}> 
          <Image style={styles.weatherIcon} source={IconImages[wState.weather[0].icon]}/>
          <Image onPress={() => movePic}style={styles.turt} source={TurtImages[wState.weather[0].icon]} />
        </Animated.View>



        {/* location search */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={showLoc}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}>
          <View style={styles.locationSearch}>
            <LocationSearch locBoolean={locBoolean} getData={getData}/>

            <TouchableOpacity onPress={() => locBoolean()} style={styles.downarrowIconLoc}>
              <Image reverse source={require('./assets/icons/downarrow.png')} style={styles.downarrowIcon}/>
            </TouchableOpacity>

          </View>   
        </Modal>


        {/* about */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={showAbout}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}>
          <View style={styles.locationSearch}>

            <Text style={styles.aboutText}>
              About:
            </Text>
            <Text style={styles.aboutText}>
              Created by Anders T. Nicolaysen
            </Text>
            <Text style={styles.aboutTextExp} onPress={() => Linking.openURL('http://witty-fiction.surge.sh/')}>
              See other projects here
            </Text>

            <Text style={styles.aboutText}>
              (Try tapping on the sun..)
            </Text>

            <Text style={styles.aboutTextIcons}>
              Icons by Freepik from www.FlatIcon.com
            </Text>




            <Text style={styles.aboutTextExp} onPress={() => Linking.openURL('http://laughable-space.surge.sh')}>
              Privacy Policy
            </Text>
            

            <TouchableOpacity onPress={() => aboutBoolean()} style={styles.downarrowIconLoc}>
              <Image reverse source={require('./assets/icons/downarrow.png')} style={styles.downarrowIcon}/>
            </TouchableOpacity>

          </View>   
        </Modal>

      </ImageBackground>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
  },

  weatherIcon: {
    height: 75,
    width: 75,
    marginTop: 20,
    marginBottom: -7
  },

  backgroundImage: {
    alignItems: 'center',
    justifyContent: 'center',
    height:'102.5%',
    width: '100%',
    marginBottom: '-50%'
  },

  turtbox: {
    position: 'absolute',
    left: '60%',
    bottom: '4%'
  },

  turt: {
    marginLeft: '-10%',
    marginTop: '-20%'
  },

  mainText: {
    marginTop: 50,
    marginBottom: 200
  },

  forecast: {
    flexDirection: "row",
    marginBottom: '10%'
  },

   forecastText: {
    paddingTop: '5%',
    paddingHorizontal: '4%',
    fontWeight:'800'
  },

  mainText: {
    fontWeight:'800'
  },

  forecastHeader: {
    paddingTop: '5%',
    paddingHorizontal: '2%'
  },

  weatherIconSmall: {
    height: 35,
    width: 35,
    marginTop: 5,
    marginLeft: 20,
  },

  earthIconLoc: {
    position: "absolute",
    top: '5%',
    left: '5%'
  },

  earthIcon: {
    width: 50,
    height: 50,
  },

  questIconLoc: {
    position: "absolute",
    bottom: '-15%',
    right: '5%'
  },

  questIcon: {
    width: 32,
    height: 32,
  },

  downarrowIconLoc: {
    position: "absolute",
    top: '-30%'
  },

  downarrowIcon: {
    width: 96,
    height: 96,
  },

  aboutText: {
    marginTop: 10
  },

  aboutTextExp: {
    marginTop: 5,
    textDecorationLine: 'underline',
  },

  aboutTextIcons: {
    marginTop: 20,
  },

  grassText: {
    position: 'absolute',
    bottom: -180,
    left: -180,
    color: 'green'
  },

  wishText: {
    position: 'absolute',
    bottom: -90,
    left: -100,
    color: 'darkblue'
  },

  locationSearch: {
    flex: 1,
    marginTop: '50%',
    backgroundColor: 'rgba(255,255,255,0.9)',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: '5%',
    marginBottom: '10%',
    borderRadius: 30,
    borderWidth: 1,
  },

  //! adds 

  sunglasses: {
    width: 128,
    height: 64,
    position: 'absolute',
    right: -10,
    top: 45, 
  },

  blank: {
    width: 128,
    height: 128,
    position: 'absolute',
    right: 10,
    top: 20, 
  },

  smile1: {
    width: 96,
    height: 96,
    position: 'absolute',
    right: 5,
    top: 25, 
  },

  smile2: {
    width: 96,
    height: 96,
    position: 'absolute',
    left: -180,
    top: 55, 
  },


});
